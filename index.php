<?php 

// require_once('kendaraan.php');
// require_once('motor.php');


// $object = new Kendaraan("mobil");

// echo "Nama Kendaraan :" . $object->name . "<br>";
// echo "Jumlah Roda :" . $object ->  roda . "<br>";
// echo "Bahan Bakar Bensin :" . $object ->  bensin . "<br>";

// echo "<br></br>";

// $object2 = new Motor("motor");
// echo "Nama Kendaraan :" . $object2->name . "<br>";
// echo "Jumlah Roda : ". $object2->roda ."<br>";
// echo "Bahan Bakar Bensin : ". $object2->bensin. "<br>";

// $object2->jalan();

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');


$sheep = new animal("shaun");

echo "Name : " . $sheep->name . "<br>";
echo "Legs : " . $sheep ->  legs . "<br>";
echo "Cold Blooded : " . $sheep ->  cold_blooded . "<br>";

echo "<br></br>";

$kodok = new frog("Buduk");
echo "Name :" . $kodok->name . "<br>";
echo "Legs : ". $kodok->legs ."<br>";
echo "Cold Blooded : ". $kodok->cold_blooded. "<br>";
$kodok->jump() ;

echo "<br></br>";

$sungokong = new ape("Kera Sakti");
echo "Name : " . $sungokong->name . "<br>";
echo "Legs : ". $sungokong->legs ."<br>";
echo "Cold Blooded : ". $sungokong->cold_blooded. "<br>";
$sungokong->yell() ;